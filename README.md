# watchnext.tk
![aissame boukhallad](logo.jpg)
<!-- <p align="center">
  <img src="app.jpg" width="350" title="hover text">
  <img src="logo.jpg" width="350" alt="accessibility text">
</p> -->
![aissame boukhallad](respensive-min.png)
Le projet réside dans la conception et la réalisation d’une application web nommée « WATCH NEXT». Cette application a pour but de mettre en œuvre les différentes informations des films, des séries télévisées et des acteurs de cinéma. Elle doit implémenter les processus de consultation détaillée des informations à propos des films et séries télévisées (acteurs, date production, trailers, …) ainsi que la favorisation du contenu aux visiteurs leur permettant ainsi d’ajouter facilement un film à leurs favoris. Le site doit également permettre aux visiteurs de créer leurs propres comptes et de s’y connecter ultérieurement. Cette application viendra enrichir l’expérience de divertissement des utilisateurs à travers une navigation fluide simple et appétissante.

## Comment Installer

1. Clone the repo and `cd` into it
1. `composer install`
1. cd `.env`
1. Set your `TMDB_TOKEN` in your `.env` file. You can get an API key [here](https://www.themoviedb.org/documentation/api). Make sure to use the "API Read Access Token (v4 auth)" from the TMDb dashboard.
1. `php artisan key:generate`
1. `php artisan serve` or use Laravel Valet or Laravel Homestead
1. Visit `localhost:8000` in your browser

